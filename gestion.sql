﻿
DROP DATABASE IF EXISTS b20190529;
CREATE DATABASE b20190529;
USE b20190529;
CREATE TABLE cliente(
  dni int AUTO_INCREMENT,
  apellidos varchar(100),
  nombre varchar(100),
  fecnac date,
  telefono varchar(9),
  PRIMARY KEY(dni)
  );
CREATE TABLE producto(
  cod int AUTO_INCREMENT,
  nombre varchar(100),
  precio float,
  PRIMARY KEY (cod)
  );
CREATE TABLE proveedor(
  nif int AUTO_INCREMENT,
  nombre varchar(100),
  direccion varchar(100),
PRIMARY KEY (nif)
  );
CREATE TABLE compra(
  dniclien int,
  codproducto int,
  PRIMARY KEY (dniclien, codproducto),
  CONSTRAINT fkcompracliente FOREIGN KEY (dniclien) REFERENCES cliente (dni),
  CONSTRAINT fkcompraproducto FOREIGN KEY (codproducto) REFERENCES producto (cod)
  );

CREATE TABLE suministra(
  codproducto int,
  nifproveedor int,
  PRIMARY KEY (codproducto, nifproveedor),
  CONSTRAINT fksuministraproducto FOREIGN KEY (codproducto) REFERENCES producto (cod),
  CONSTRAINT suministraproveedor FOREIGN KEY (nifproveedor) REFERENCES proveedor (nif)
  );

